//27/05/18 Adicionei a biblioteca do meu LCD

///////////////////////////////////////////////////////////////////////////////////////
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPO SE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#include <LiquidCrystal.h>
#include <Wire.h>
#include "LedControl.h"
#include <binary.h>

//variáveis matriz
LedControl lc = LedControl(7,6,13,1);

//Inicialização da biblioteca do display com declaração dos pinos
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

              
byte posXn4[8] = {B10000000,
                  B10000000, 
                  B10000000,
                  B10000000,
                  B10000000,
                  B10000000,
                  B10000000,
                  B10000000};
                  
byte posXn3[8] = {B11000000,
                  B11000000, 
                  B11000000,
                  B11000000,
                  B11000000,
                  B11000000,
                  B11000000,
                  B11000000};

byte posXn2[8] = {B01100000,
                  B01100000, 
                  B01100000,
                  B01100000,
                  B01100000,
                  B01100000,
                  B01100000,
                  B01100000};

byte posXn1[8] = {B00110000,
                  B00110000, 
                  B00110000,
                  B00110000,
                  B00110000,
                  B00110000,
                  B00110000,
                  B00110000};

byte posX0[8] = {B00011000,
                 B00011000, 
                 B00011000,
                 B00011000,
                 B00011000,
                 B00011000,
                 B00011000,
                 B00011000};

byte posX1[8] = {B00001100,
                 B00001100, 
                 B00001100,
                 B00001100,
                 B00001100,
                 B00001100,
                 B00001100,
                 B00001100};

byte posX2[8] = {B00000110,
                 B00000110, 
                 B00000110,
                 B00000110,
                 B00000110,
                 B00000110,
                 B00000110,
                 B00000110};

byte posX3[8] = {B00000011,
                 B00000011, 
                 B00000011,
                 B00000011,
                 B00000011,
                 B00000011,
                 B00000011,
                 B00000011};

byte posX4[8] = {B00000001,
                 B00000001, 
                 B00000001,
                 B00000001,
                 B00000001,
                 B00000001,
                 B00000001,
                 B00000001};

byte posYn4[8] = {B11111111,
                  B00000000,
                  B00000000,
                  B00000000,
                  B00000000,
                  B00000000,
                  B00000000,
                  B00000000};

byte posYn3[8] = {B11111111,
                  B11111111,
                  B00000000,
                  B00000000,
                  B00000000,
                  B00000000,
                  B00000000,
                  B00000000};

byte posYn2[8] = {B00000000,
                  B11111111,
                  B11111111,
                  B00000000,
                  B00000000,
                  B00000000,
                  B00000000,
                  B00000000};

byte posYn1[8] = {B00000000,
                  B00000000,
                  B11111111,
                  B11111111,
                  B00000000,
                  B00000000,
                  B00000000,
                  B00000000};

byte posY0[8] = {B00000000,
                 B00000000,
                 B00000000,
                 B11111111,
                 B11111111,
                 B00000000,
                 B00000000,
                 B00000000};

byte posY1[8] = {B00000000,
                 B00000000,
                 B00000000,
                 B00000000,
                 B11111111,
                 B11111111,
                 B00000000,
                 B00000000};

byte posY2[8] = {B00000000,
                 B00000000,
                 B00000000,
                 B00000000,
                 B00000000,
                 B11111111,
                 B11111111,
                 B00000000};

byte posY3[8] = {B00000000,
                 B00000000,
                 B00000000,
                 B00000000,
                 B00000000,
                 B00000000,
                 B11111111,
                 B11111111};
                 
byte posY4[8] = {B00000000,
                 B00000000,
                 B00000000,
                 B00000000,
                 B00000000,
                 B00000000,
                 B00000000,
                 B11111111};                

//float grauY = 30 ; //[-90; 90]
//float grauX = 30; //[-90; 90]

byte posXatual[8];
byte posYatual[8];
byte matrizLed[8];


//Variaveis globais
int gyro_x, gyro_y, gyro_z;
long acc_x, acc_y, acc_z, acc_total_vector;
int temperature;
long gyro_x_cal, gyro_y_cal, gyro_z_cal;
long loop_timer;
int lcd_loop_counter;
float angle_pitch, angle_roll;
int angle_pitch_buffer, angle_roll_buffer;
boolean set_gyro_angles;
float angle_roll_acc, angle_pitch_acc;
float angle_pitch_output, angle_roll_output;

void copyArray(byte newArray[8], byte arrayCopy[8]){
  for(int i = 0; i < 8; i++){
    newArray[i] = arrayCopy[i];
  }
}

void setup() {

  //O MAX72XX sai do modo de economia de energia
  lc.shutdown(0, false);
  //Coloca a intensidade dos Leds em 8 de 15
  lc.setIntensity(0, 15);
  //Limpa o Display
  lc.clearDisplay(0);
  
  Wire.begin();                                                        //Começando comunicação I2C
  Serial.begin(57600);                                               //Uncomment se for debuggar
  pinMode(13, OUTPUT);                                                 //LED_BULTIN como output até que a calibração complete
  
  setup_mpu_6050_registers();                                          //Usando MPU, começando os registers do Gyro

  digitalWrite(13, HIGH);                                              //Digital 13 alto para começar o Gyro

  lcd.begin(16, 2);                                                    //Inicializar e limpar o LCD
  lcd.clear();

  lcd.setCursor(0,0);                                                  //Algum texto na tela
  lcd.print("Autocalibracao");                                         
  lcd.setCursor(0,1);                                                  
  lcd.print("1.2");                                              

  delay(1500);                                                         //Delay de 1.5s e limpa o LCD
  lcd.clear();                                                       
  
  lcd.setCursor(0,0);                                                 
  lcd.print("Calibrando...");                                       
  lcd.setCursor(0,1);                                                  
  for (int cal_int = 0; cal_int < 2000 ; cal_int ++){                  
    if(cal_int % 125 == 0)lcd.print(".");                              
    read_mpu_6050_data();                                              //Lendo o raw do Gyro
    gyro_x_cal += gyro_x;                                              
    gyro_y_cal += gyro_y;                                             
    gyro_z_cal += gyro_z;                                              
    delay(3);                                                          //Delay de 3microseg para evitar bits errados
  }
  gyro_x_cal /= 2000;                                                  //Dividindo o average offset por 2000
  gyro_y_cal /= 2000;                                                  
  gyro_z_cal /= 2000;                                                  

  lcd.clear();                                                         
  
  lcd.setCursor(0,0);                                                  
  lcd.print("X:");                                                 
  lcd.setCursor(0,1);                                                  
  lcd.print("Y:");                                                 
  
  digitalWrite(13, LOW);                                               //Desligue o LED_BUILTIN
  
  loop_timer = micros();                                               
}

void loop(){

  read_mpu_6050_data();                                                //Read the raw acc and gyro data from the MPU-6050

  gyro_x -= gyro_x_cal;                                                //Subtract the offset calibration value from the raw gyro_x value
  gyro_y -= gyro_y_cal;                                                //Subtract the offset calibration value from the raw gyro_y value
  gyro_z -= gyro_z_cal;                                                //Subtract the offset calibration value from the raw gyro_z value
  
  //Gyro angle calculations
  //0.0000611 = 1 / (250Hz / 65.5)
  angle_pitch += gyro_x * 0.0000611;                                   //Calculate the traveled pitch angle and add this to the angle_pitch variable
  angle_roll += gyro_y * 0.0000611;                                    //Calculate the traveled roll angle and add this to the angle_roll variable
  
  //0.000001066 = 0.0000611 * (3.142(PI) / 180degr) The Arduino sin function is in radians
  angle_pitch += angle_roll * sin(gyro_z * 0.000001066);               //If the IMU has yawed transfer the roll angle to the pitch angel
  angle_roll -= angle_pitch * sin(gyro_z * 0.000001066);               //If the IMU has yawed transfer the pitch angle to the roll angel
  
  //Accelerometer angle calculations
  acc_total_vector = sqrt((acc_x*acc_x)+(acc_y*acc_y)+(acc_z*acc_z));  //Calculate the total accelerometer vector
  //57.296 = 1 / (3.142 / 180) The Arduino asin function is in radians
  angle_pitch_acc = asin((float)acc_y/acc_total_vector)* 57.296;       //Calculate the pitch angle
  angle_roll_acc = asin((float)acc_x/acc_total_vector)* -57.296;       //type: FLOAT
  //Place the MPU-6050 spirit level and note the values in the following two lines for calibration
  angle_pitch_acc -= 0.0;                                              //Accelerometer calibration value for pitch
  angle_roll_acc -= 0.0;                                               //Accelerometer calibration value for roll

  if(set_gyro_angles){                                                 //If the IMU is already started
    angle_pitch = angle_pitch * 0.9996 + angle_pitch_acc * 0.0004;     //Correct the drift of the gyro pitch angle with the accelerometer pitch angle
    angle_roll = angle_roll * 0.9996 + angle_roll_acc * 0.0004;        //Correct the drift of the gyro roll angle with the accelerometer roll angle
  }
  else{                                                                //At first start
    angle_pitch = angle_pitch_acc;                                     //Set the gyro pitch angle equal to the accelerometer pitch angle 
    angle_roll = angle_roll_acc;                                       //Set the gyro roll angle equal to the accelerometer roll angle 
    set_gyro_angles = true;                                            //Set the IMU started flag
  }
  
  //To dampen the pitch and roll angles a complementary filter is used -- 
  angle_pitch_output = angle_pitch_output * 0.9 + angle_pitch * 0.1;   //Take 90% of the output pitch value and add 10% of the raw pitch value
  angle_roll_output = angle_roll_output * 0.9 + angle_roll * 0.1;      //Take 90% of the output roll value and add 10% of the raw roll value
  
  write_LCD();                                                         //Write the roll and pitch values to the LCD display

  while(micros() - loop_timer < 4000);                                 //Wait until the loop_timer reaches 4000us (250Hz) before starting the next loop
  loop_timer = micros();                                               //Reset the loop timer

  //Matriz
    //Verifica a posição que está em X
  if(angle_pitch_output < -72){
    copyArray(posXatual, posXn4);
  } else if (angle_pitch_output < -54){
    copyArray(posXatual, posXn3);
  } else if (angle_pitch_output < -36){
    copyArray(posXatual, posXn2);
  } else if (angle_pitch_output < -18){
    copyArray(posXatual, posXn1);
  } else if (angle_pitch_output < 18){
    copyArray(posXatual, posX0);
  } else if (angle_pitch_output < 36){
    copyArray(posXatual, posX1);
  } else if (angle_pitch_output < 54){
    copyArray(posXatual, posX2);
  } else if (angle_pitch_output < 72){
    copyArray(posXatual, posX3);
  } else if (angle_pitch_output <= 90){
    copyArray(posXatual, posX4);
  }

  //Verifica a posição em Y
  if(angle_roll_output < -72){
    copyArray(posYatual, posYn4);
  } else if (angle_roll_output < -54){
    copyArray(posYatual, posYn3);
  } else if (angle_roll_output < -36){
    copyArray(posYatual, posYn2);
  } else if (angle_roll_output < -18){
    copyArray(posYatual, posYn1);
  } else if (angle_roll_output < 18){
    copyArray(posYatual, posY0);
  } else if (angle_roll_output < 36){
    copyArray(posYatual, posY1);
  } else if (angle_roll_output < 54){
    copyArray(posYatual, posY2);
  } else if (angle_roll_output < 72){
    copyArray(posYatual, posY3);
  } else if (angle_roll_output <= 90){
    copyArray(posYatual, posY4);
  }

  //Cria o Vetor com os Leds que devem estar ligados
  for(int i = 0; i < 8; i++){
    matrizLed[i] = posXatual[i] & posYatual[i];
  }

  //Passa para matriz de Leds (ACENDE)
  for(int j = 0; j < 8; j++){
    lc.setRow(0, j, matrizLed[j]);
  }
  
  //delay(100);
}


void read_mpu_6050_data(){                                             //Subroutine for reading the raw gyro and accelerometer data
  Wire.beginTransmission(0x68);                                        //Start communicating with the MPU-6050
  Wire.write(0x3B);                                                    //Send the requested starting register
  Wire.endTransmission();                                              //End the transmission
  Wire.requestFrom(0x68,14);                                           //Request 14 bytes from the MPU-6050
  while(Wire.available() < 14);                                        //Wait until all the bytes are received
  acc_x = Wire.read()<<8|Wire.read();                                  //Add the low and high byte to the acc_x variable
  acc_y = Wire.read()<<8|Wire.read();                                  //Add the low and high byte to the acc_y variable
  acc_z = Wire.read()<<8|Wire.read();                                  //Add the low and high byte to the acc_z variable
  temperature = Wire.read()<<8|Wire.read();                            //Add the low and high byte to the temperature variable
  gyro_x = Wire.read()<<8|Wire.read();                                 //Add the low and high byte to the gyro_x variable
  gyro_y = Wire.read()<<8|Wire.read();                                 //Add the low and high byte to the gyro_y variable
  gyro_z = Wire.read()<<8|Wire.read();                                 //Add the low and high byte to the gyro_z variable

}

void write_LCD(){                                                      //Subroutine for writing the LCD
  //To get a 250Hz program loop (4us) it's only possible to write one character per loop
  //Writing multiple characters is taking to much time
  if(lcd_loop_counter == 14)lcd_loop_counter = 0;                      //Reset the counter after 14 characters
  lcd_loop_counter ++;                                                 //Increase the counter
  if(lcd_loop_counter == 1){
    angle_pitch_buffer = angle_pitch_output * 10;                      //Buffer the pitch angle because it will change
    lcd.setCursor(6,0);                                                //Set the LCD cursor to position to position 0,0
  }
  if(lcd_loop_counter == 2){
    if(angle_pitch_buffer < 0)lcd.print("-");                          //Print - if value is negative
    else lcd.print("+");                                               //Print + if value is negative
  }
  if(lcd_loop_counter == 3)lcd.print(abs(angle_pitch_buffer)/1000);    //Print first number
  if(lcd_loop_counter == 4)lcd.print((abs(angle_pitch_buffer)/100)%10);//Print second number
  if(lcd_loop_counter == 5)lcd.print((abs(angle_pitch_buffer)/10)%10); //Print third number
  if(lcd_loop_counter == 6)lcd.print(".");                             //Print decimal point
  if(lcd_loop_counter == 7)lcd.print(abs(angle_pitch_buffer)%10);      //Print decimal number

  if(lcd_loop_counter == 8){
    angle_roll_buffer = angle_roll_output * 10;
    lcd.setCursor(6,1);
  }
  if(lcd_loop_counter == 9){
    if(angle_roll_buffer < 0)lcd.print("-");                           //Print - if value is negative
    else lcd.print("+");                                               //Print + if value is negative
  }
  if(lcd_loop_counter == 10)lcd.print(abs(angle_roll_buffer)/1000);    //Print first number
  if(lcd_loop_counter == 11)lcd.print((abs(angle_roll_buffer)/100)%10);//Print second number
  if(lcd_loop_counter == 12)lcd.print((abs(angle_roll_buffer)/10)%10); //Print third number
  if(lcd_loop_counter == 13)lcd.print(".");                            //Print decimal point
  if(lcd_loop_counter == 14)lcd.print(abs(angle_roll_buffer)%10);      //Print decimal number
}

void setup_mpu_6050_registers(){
  //Activate the MPU-6050
  Wire.beginTransmission(0x68);                                        //Start communicating with the MPU-6050
  Wire.write(0x6B);                                                    //Send the requested starting register
  Wire.write(0x00);                                                    //Set the requested starting register
  Wire.endTransmission();                                              //End the transmission
  //Configure the accelerometer (+/-8g)
  Wire.beginTransmission(0x68);                                        //Start communicating with the MPU-6050
  Wire.write(0x1C);                                                    //Send the requested starting register
  Wire.write(0x10);                                                    //Set the requested starting register
  Wire.endTransmission();                                              //End the transmission
  //Configure the gyro (500dps full scale)
  Wire.beginTransmission(0x68);                                        //Start communicating with the MPU-6050
  Wire.write(0x1B);                                                    //Send the requested starting register
  Wire.write(0x08);                                                    //Set the requested starting register
  Wire.endTransmission();                                              //End the transmission
}














